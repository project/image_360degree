/**
 * @file
 * Creates a PhotoSphereViewer with given settings.
 */

jQuery(document).ready(function ($) {
  'use strict';
  // Loop through all image360Degree settings.
  for (var index in Drupal.settings.image360Degree) {
    if ({}.hasOwnProperty.call(Drupal.settings.image360Degree, index)) {
      // Get the 360 degree div.
      this.div = document.getElementById('photo_sphere_viewer_' + index);
      // Create the PhotoSphereViewer which animates the 360 degree view.
      window['PSV' + index] = new PhotoSphereViewer({
        panorama: Drupal.settings.image360Degree[index].uri,
        container: this.div,
        navbar: Drupal.settings.image360Degree[index].navbar,
        navbar_style: {
          backgroundColor: Drupal.settings.image360Degree[index].backgroundColor,
          buttonsColor: Drupal.settings.image360Degree[index].buttonsColor,
          buttonsBackgroundColor: Drupal.settings.image360Degree[index].buttonsBackgroundColor,
          activeButtonsBackgroundColor: Drupal.settings.image360Degree[index].activeButtonsBackgroundColor,
          buttonsHeight: Drupal.settings.image360Degree[index].buttonsHeight,
          autorotateThickness: Drupal.settings.image360Degree[index].autorotateThickness,
          zoomRangeWidth: Drupal.settings.image360Degree[index].zoomRangeWidth,
          zoomRangeThickness: Drupal.settings.image360Degree[index].zoomRangeThickness,
          zoomRangeDisk: Drupal.settings.image360Degree[index].zoomRangeDisk,
          fullscreenRatio: Drupal.settings.image360Degree[index].fullscreenRatio,
          fullscreenThickness: Drupal.settings.image360Degree[index].fullscreenThickness
        },
        loading_msg: Drupal.settings.image360Degree[index].loadingMsg
      });
    }
  }
});
